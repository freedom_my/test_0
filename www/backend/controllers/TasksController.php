<?php

namespace backend\controllers;

use backend\models\TaskSearch;
use yii;
use backend\models\Tasks;
use yii\web\NotFoundHttpException;

class TasksController extends \yii\web\Controller
{
    public function actionCreate()
    {
        $model = new Tasks();
        $statusList = Tasks::STATUS_LIST;
        $priorityList = Tasks::PRIORITY_LIST;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create',[
                'model' => $model,
                'statusList' => $statusList,
                'priorityList' => $priorityList
            ]]);
        } else{
            return $this->render('create',[
                'model' => $model,
                'statusList' => $statusList,
                'priorityList' => $priorityList
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $statusList = Tasks::STATUS_LIST;
        $priorityList = Tasks::PRIORITY_LIST;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['list']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'statusList' => $statusList,
                'priorityList' => $priorityList
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['list']);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->status = Tasks::STATUS_LIST[$model->status];

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionList()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}