<?php

namespace backend\models;

class Tasks extends \yii\db\ActiveRecord
{
    const STATUS_LIST = [
       0 => 'Создана',
       1 => 'В работе',
       2 => 'Выполнена',
    ];

    const PRIORITY_LIST = [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5
    ];

    public static function tableName()
    {
        return 'tasks';
    }

    public function rules(){
        return [
            [['name','status','description','priority'],'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'name' => 'Название',
            'description' => 'Описание',
            'priority' => 'Приоритет',
            'created_at' => 'Создано',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->setCreatedAt();
            }
            return true;
        }
        return false;
    }

    public function setCreatedAt()
    {
        $this->created_at = date("Y-m-d H:i:s");
    }
}