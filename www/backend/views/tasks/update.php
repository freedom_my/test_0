<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'priority')->dropDownList(
        $priorityList ,
        ['prompt' => 'Выберите приоритет']
    );?>
    <?= $form->field($model, 'status')->dropDownList(
        $statusList ,
        ['prompt' => 'Выберите статус']
    );?>

    <div class="form-group">
        <?= Html::submitButton('Изменить', ['class' =>  'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>