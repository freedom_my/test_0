<?php
use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'name',
        'description',
        'priority',
        'status',
        'created_at:datetime',
    ],
]);