<?php
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
?>

<h1>Список задач</h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'name',
        'description',
        'status',
        'priority',
        'created_at',
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>